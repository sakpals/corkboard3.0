const mongoose = require("mongoose");
require("dotenv").config();

const connect = () => {
  if (process.env.MONGO_URI) {
    const db = process.env.MONGO_URI;
    mongoose
      .connect(db, { useNewUrlParser: true })
      .then(() => console.log("Connected to db"))
      .catch((err) => console.log(`Failed to connect to db: ${err}`));
  }
};

module.exports = { connect };
