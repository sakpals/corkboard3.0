const express = require("express");
const db = require("./db");
const helmet = require("helmet");
const path = require("path");
const bodyParser = require("body-parser");
const app = express();

const buildLocation = path.join(__dirname, "..", "corkboard", "build");

// DB setup
db.connect();

app.use(helmet());

// Configure the app to use bodyParser()
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());

// Serve static content in production
if (process.env.NODE_ENV === "production")
  app.use(express.static(buildLocation));

app.get("/*", function (req, res) {
  res.sendFile(
    path.resolve(__dirname, "..", "corkboard", "build", "index.html")
  );
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
