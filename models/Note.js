const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// defines schema that is used to save a document
const NoteSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  body: {
    type: String
  },
  date: {
    type: Date,
    required: true,
    default: new Date()
  }
});

// model is a class that constructs documents
let Note = mongoose.model("Note", NoteSchema, "notes");
module.exports = Note;
