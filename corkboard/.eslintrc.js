module.exports = {
  parser: "@typescript-eslint/parser", // ESLint parser to parse TS code
  extends: [
    "plugin:@typescript-eslint/recommended", // Use recommended ESLint rules specific to TS
    "plugin:react/recommended", // Use recommended ESLint rules for React from eslint-plugin-react
    "plugin:prettier/recommended", // Use recommended Eslint rules from eslint-plugin-prettier (should be last in this array)
  ],
  parserOptions: {
    project: "./tsconfig.json",
    // ecmaVersion: 11, // Parse modern ECMAScript features
    sourceType: "module", // Allow use of imports
    jsx: true,
  },
  rules: {
    "@typescript-eslint/explicit-function-return-type": "off",
  },
  settings: {
    react: {
      version: "detect", // Automatically let eslint-plugin-react to detect version of React
    },
  },
};
