export const homePage = {
  profile: {
    title: "Hi, I'm Sampada.",
    body: [
      "I’m a full stack software developer based in Melbourne, Australia.",
      " I build web and mobile apps. Currently working at IBM Garage for" +
        " Cloud.",
      "Ask me about it!",
    ],
  },
};
