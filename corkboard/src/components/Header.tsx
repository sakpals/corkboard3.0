import * as React from "react";
import { AppBar, Toolbar, Typography } from "@material-ui/core";

export function Header() {
  return (
    <AppBar position="static">
      <Toolbar>
        <Typography style={{ color: "#fff" }}>Sampada Sakpal</Typography>
      </Toolbar>
    </AppBar>
  );
}
