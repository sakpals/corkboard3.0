import React from "react";
import { LinkedIn, GitHub, Email } from "@material-ui/icons";
import { GitLabIcon } from "../theme/icons";
import { IconButton } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
/* eslint @typescript-eslint/no-unused-vars: "off" */
const useStyles = makeStyles((theme) => ({
  iconRow: {
    display: "flex",
  },
}));

export default function Contact() {
  const classes = useStyles();
  return (
    <div className={classes.iconRow}>
      <IconButton
        href="https://www.linkedin.com/in/sampada-sakpal/"
        target="_blank"
        rel="noopener"
        color="secondary"
        aria-label="go to linkedin profile"
      >
        <LinkedIn />
      </IconButton>
      <IconButton
        href="https://github.com/sakpals"
        target="_blank"
        rel="noopener"
        color="secondary"
        aria-label="go to github profile"
      >
        <GitHub />
      </IconButton>
      <IconButton
        href="https://gitlab.com/sakpals"
        target="_blank"
        rel="noopener"
        color="secondary"
        aria-label="go to gitlab profile"
      >
        <GitLabIcon />
      </IconButton>
      <IconButton
        href="mailto:smiley_sampada_sakpal@hotmail.com"
        rel="noopener"
        color="secondary"
        aria-label="send an email to Sampada"
      >
        <Email />
      </IconButton>
    </div>
  );
}
