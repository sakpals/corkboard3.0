import * as React from "react";
import { makeStyles } from "@material-ui/core/styles";
import styled from "styled-components";
import { Container, Typography, Grid } from "@material-ui/core";
import { homePage } from "./utils/content";
import Contact from "./components/Contact";
import {
  TypescriptIcon,
  JavascriptIcon,
  ReactIcon,
  ReduxIcon,
  NodeJSIcon,
  MongoDBIcon,
  PythonIcon,
  JavaIcon,
  DjangoIcon,
  FlaskIcon,
} from "./theme/icons";

const Section = styled(Container)`
  flex: 1;
  height: 100vh;
  width: 100vw;
  padding: 10%;
`;

const Row = styled(Grid)`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
const Column = styled(Grid)`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
`;

const useStyles = makeStyles((theme) => ({
  section1: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.secondary.main,
  },
  section2: {
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.primary.main,
  },
}));

export default function App() {
  const classes = useStyles();
  return (
    <div>
      <Section className={classes.section1}>
        <Row>
          <Column>
            <Typography variant="h2" gutterBottom>
              {homePage.profile.title}
            </Typography>
            {homePage.profile.body.map((line, i) => (
              <Typography key={`profile-line${i}`} variant="body1">
                {line}
              </Typography>
            ))}
            <Contact />
          </Column>
          <Column>
            <img
              src="profile.png"
              width="300px"
              alt="Profile cartoon of Sampada wearing a purple top"
            />
          </Column>
        </Row>
      </Section>
      <Section className={classes.section2}>
        <Typography variant="h2" gutterBottom>
          What I do:
        </Typography>

        <Row>
          <TypescriptIcon />
          <JavascriptIcon />
          <ReactIcon />
          <ReduxIcon />
          <NodeJSIcon />
          <MongoDBIcon />
          <JavaIcon />
          <PythonIcon />
          <DjangoIcon />
          <FlaskIcon />
        </Row>
        <Row>
          <img
            src="logo_ckad.png"
            alt="Certified Kubernetes Application Developer"
            width="128px"
            height="128px"
          />
        </Row>
      </Section>
    </div>
  );
}
