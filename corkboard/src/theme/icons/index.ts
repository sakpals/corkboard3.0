import { TypescriptIcon } from "./Typescript";
import { JavascriptIcon } from "./Javascript";
import { ReactIcon } from "./React";
import { ReduxIcon } from "./Redux";
import { NodeJSIcon } from "./NodeJS";
import { MongoDBIcon } from "./MongoDB";
import { PythonIcon } from "./Python";
import { JavaIcon } from "./Java";
import { DjangoIcon } from "./Django";
import { FlaskIcon } from "./Flask";
import { GitLabIcon } from "./GitLab";

export {
  TypescriptIcon,
  JavascriptIcon,
  ReactIcon,
  ReduxIcon,
  NodeJSIcon,
  MongoDBIcon,
  PythonIcon,
  JavaIcon,
  DjangoIcon,
  FlaskIcon,
  GitLabIcon,
};
