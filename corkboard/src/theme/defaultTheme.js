import { createMuiTheme } from "@material-ui/core/styles";

const defaultTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#1e2357",
    },
    secondary: {
      main: "#bec2e6",
    },
    common: {
      white: "#ffffff",
    },
  },
  typography: {
    fontFamily: "'Didact Gothic', sans-serif",
  },
  overrides: {
    MuiAppBar: { root: { height: "64px" } },
    MuiToolbar: {
      gutters: { paddingLeft: 30, paddingRight: 30 },
    },
    MuiTypography: {
      body1: {
        fontFamily: "'Open Sans', sans-serif",
        lineHeight: 1.8,
      },
    },
  },
});

const theme = {
  ...defaultTheme,
};

export default theme;
